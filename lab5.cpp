/*
Will Kerr
wqkerr
Lab 5
Lab Section 3
*/
#include <sstream>
#include <iostream>
#include <iomanip>
#include <algorithm>
#include <sstream>
#include <string>
#include "stdlib.h"
#include "string.h"
using namespace std;
enum Suit { SPADES=0, HEARTS=1, DIAMONDS=2, CLUBS=3 };

typedef struct Card {
  Suit suit;
  int value;
} Card;

string get_suit_code(Card& c);
string get_card_name(Card& c);
bool suit_order(const Card& lhs, const Card& rhs);
int myrandom (int i) { return std::rand()%i;}


int main(int argc, char const *argv[]) {
  int i = 0;
  int j = 0;
  int z = 0;
  int num = 0;
  // IMPLEMENT as instructed below
  /*This is to seed the random generator */
  srand(unsigned (time(0)));

  /*Create a deck of cards of size 52 (hint this should be an array) and
   *initialize the deck*/

   Card fullDeck[52];
   for(i = 0; i < 4; i++)
   {
     for(j = 1; j < 14; j++)
     {
       fullDeck[num].suit = (Suit) i;
       fullDeck[num].value = j;
       num++;
     }
   }
   num = 0;

  /*After the deck is created and initialzed we call random_shuffle() see the
   *notes to determine the parameters to pass in.*/

   random_shuffle(fullDeck, fullDeck + 52, myrandom);

   /*Build a hand of 5 cards from the first five cards of the deck created
    *above*/

    Card hand[5];
    for(i = 0; i < 5; i++)
    {
      hand[i] = fullDeck[i];
    }

    cout <<"\n";
    
    /*Sort the cards.  Links to how to call this function is in the specs
     *provided*/

    sort(hand, hand+5, suit_order);
    
     for(z = 0; z < 5; z++){
       cout << setw(10) << get_card_name(hand[z])
	    << get_suit_code(hand[z]) << endl;
     }

    /*Now print the hand below. You will use the functions get_card_name and
     *get_suit_code */




  return 0;
}


/*This function will be passed to the sort funtion. Hints on how to implement
* this is in the specifications document.*/
bool suit_order(const Card& lhs, const Card& rhs) {
  // IMPLEMENT
  bool wk = false;

  //if statements compare cards
  if(lhs.suit < rhs.suit)
  {
    wk = true;
  }
  if(rhs.suit < lhs.suit)
  {
    if(rhs.value == lhs.value)
    {
      wk = true;
    }
  }

return wk;
}

//This function uses a switch statement to retrieve the suit symbol 
string get_suit_code(Card& c) {
  switch (c.suit) {
    case SPADES:    return "\u2660";
    case HEARTS:    return "\u2661";
    case DIAMONDS:  return "\u2662";
    case CLUBS:     return "\u2663";
    default:        return "";
  }
}

//This function names the face cards using a switch statement
string get_card_name(Card& c) {
  // IMPLEMENT
  switch (c.value){
    case 11:  return "Jack of ";
    case 12:  return "Queen of ";
    case 13:  return "King of ";
    case 14:  return "Ace of ";
  default: return to_string(c.value)+ " of ";   
  }
}
